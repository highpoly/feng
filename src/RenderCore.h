#pragma once

//OpenGL includes
#include "OpenGLIncludes.h"

//Windows includes
#include "WindowsIncludes.h"

//Other includes
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "ShaderLoading.h"
#include "SceneData.h"
#include "Camera.h"
#include "ObjectData.h"


static ProgramData programData;
static SceneData mainScene;
static CameraData defaultCamera;




GLvoid renderMainScene();
GLvoid startRendering();
GLvoid finishRendering();
