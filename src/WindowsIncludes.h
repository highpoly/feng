/*
    This header contains
    Windows includes needed
    to work with WinAPI, wgl functions,
    GDI, etc.
*/


#pragma once

#include <Windows.h>
#include <Wingdi.h>
#include <Windowsx.h>
