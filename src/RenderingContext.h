//OpenGL includes
#include <gl\glew.h>
#include <gl\gl.h>
#include <gl\glu.h>

//Windows includes
#include <Windows.h>
#include <Wingdi.h>

//Other includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void CreateRenderingContext(HWND hWnd);
void DeleteRenderingContext(HWND hWnd);
void PixelFormatInitialization(HDC hdc);
void DisplayLoop(HWND hWnd);
