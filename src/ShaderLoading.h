#pragma once

//OpenGL includes
#include <gl\glew.h>
#include <gl\gl.h>
#include <gl\glu.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


typedef struct {
    
    GLuint shaderID;
    GLenum shaderType;
    char* fileName;

} ShaderData;

typedef struct {

    GLuint programID;

} ProgramData;




//Program service functions
//--------------------------------
GLboolean deleteProgram(ProgramData* program);
GLboolean createProgram(ProgramData* program, int numOfShaders, ShaderData* shaders);
GLboolean linkProgram(ProgramData* program);
GLboolean detachShaders(ProgramData* program);
GLboolean attachShaders(ProgramData* program, int numOfShaders, ShaderData* shaders);
//--------------------------------
//Shader service functions
//--------------------------------
GLboolean createShader(ShaderData* shader);
GLboolean loadShaderSource(ShaderData* shader);
GLboolean compileShader(GLuint shaderID);
GLboolean deleteShader(GLuint shaderID);
GLboolean detachShader(ProgramData* program, GLuint shaderID);
GLboolean attachShader(ProgramData* program, GLuint shaderID);
//--------------------------------
//Other service functions
//--------------------------------
long sizeOfFile(FILE* file);
//--------------------------------
