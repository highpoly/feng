#include "RenderCore.h"




GLvoid renderMainScene() {
    
    //First clear buffers and fill screen
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);


    //Run main scene rendering
    renderScene(&mainScene);

}

GLvoid startRendering() {

    //First create program, attach
    //shaders, link and use it
    ShaderData shaders[2] = {
        { 0, GL_VERTEX_SHADER, "c:\\tasks\\repo\\glapp1\\tmp\\shaders\\vshader.vsh" },
        { 0, GL_FRAGMENT_SHADER, "c:\\tasks\\repo\\glapp1\\tmp\\shaders\\fshader.fsh" }
    };
    createProgram(&programData, 2, shaders);
    glUseProgram(programData.programID);

    
    //Initialize camera
    initCamera(&defaultCamera, &programData, "viewMatrix", "projectionMatrix");
    //Initialize frustum and update view matrix
    //updateProjectionMatrix(&defaultCamera, LEFT, TOP, RIGHT, BOTTOM, NEAR, FAR);
    updateProjectionMatrixFieldOfView(&defaultCamera, toRadians(45.0f), 16.0f / 9.0f, 1.0f, 10000.0f);
    updateViewMatrix(&defaultCamera); 
    updateModelMatrixLocation(&mainScene, &programData, "modelMatrix");
    //Next attach camera to the main scene
    attachCamera(&mainScene, &defaultCamera);


    //Enable OpenGL depth testing
    glEnable(GL_DEPTH_TEST);


    //Init OpenGL cull mode
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);
    glFrontFace(GL_CCW);
    

    createObjectList(&(mainScene.objectList));
    //Just create hello triangle
        
        
        ObjectData triangle = {
            3, 3, NULL, NULL, 1,
            0.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 0.0f,
            1.0f, 1.0f, 1.0f
        };
        VertexData vertices[3] = {
            -0.5f, -0.5f,  2.0f,  1.0f,
             0.0f,  0.5f,  2.0f,  1.0f,
             0.5f, -0.5f,  2.0f,  1.0f
        };
        GLuint surface[3] = {
            0, 1, 2
        };
        triangle.vertices = vertices;
        triangle.indices = surface;
        

        //Next attach triangle to the main scene
        attachObject(&mainScene, &triangle);
        

    //Object creation is finished

    
    prepareRenderData(&mainScene);
     
}

GLvoid finishRendering() {

    clearRenderData(&mainScene);
    glDeleteProgram(programData.programID);

}
