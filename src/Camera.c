#include "Camera.h"
#include "ShaderLoading.h"
#include "Math.h"




GLboolean initCamera(CameraData* camera, ProgramData* program, char* viewMatrixName, char* projectionMatrixName) {

    //  Fill all values to null
    memset(camera, 0, sizeof(CameraData));
    

    return (
        updateViewMatrixUniformLocation(camera, program, viewMatrixName) && 
        updateProjectionMatrixUniformLocation(camera, program, projectionMatrixName)
    );

}

GLboolean rotateCamera(CameraData* camera, GLfloat roll, GLfloat yaw, GLfloat pitch) {

    increaseAngle(&(camera->roll), roll);
    increaseAngle(&(camera->yaw), yaw);
    increaseAngle(&(camera->pitch), pitch);


    return updateViewMatrix(camera);

}

GLboolean translateCamera(CameraData* camera, GLfloat x, GLfloat y, GLfloat z) {

    camera->posX += x;
    camera->posY += y;
    camera->posZ += z;


    return updateViewMatrix(camera);

}

GLboolean updateViewMatrix(CameraData* camera) {

    //  Construct view matrix and update it
    //  through viewMatrixLocation variable
    

    VECTOR_FLOAT d;
    VECTOR_FLOAT u;
    VECTOR_FLOAT r;
    setVector(&d, createVector(3, GL_TRUE, 3, 0.0f, 0.0f, 1.0f));
    setVector(&d, rotateVector(&d, camera->pitch, camera->yaw, camera->roll));
    setVector(&u, createVector(3, GL_TRUE, 3, 0.0f, 1.0f, 0.0f));
    setVector(&r, crossProduct(&d, &u));
    setVector(&u, crossProduct(&r, &d));


    MATRIX_FLOAT* viewMatrix = createMatrix(
        3, 3, GL_FALSE, 3, 3,
        vectorGetAt(&r, 0), vectorGetAt(&r, 1), vectorGetAt(&r, 2),
        vectorGetAt(&u, 0), vectorGetAt(&u, 1), vectorGetAt(&u, 2),
        vectorGetAt(&d, 0), vectorGetAt(&d, 1), vectorGetAt(&d, 2)
    );
    viewMatrix = inverseMatrix(viewMatrix);
    viewMatrix = createMatrix(
        4, 4, GL_FALSE, 4, 4,
        matrixGetAt(viewMatrix, 0, 0), matrixGetAt(viewMatrix, 0, 1), matrixGetAt(viewMatrix, 0, 2), -camera->posX,
        matrixGetAt(viewMatrix, 1, 0), matrixGetAt(viewMatrix, 1, 1), matrixGetAt(viewMatrix, 1, 2), -camera->posY,
        matrixGetAt(viewMatrix, 2, 0), matrixGetAt(viewMatrix, 2, 1), matrixGetAt(viewMatrix, 2, 2), -camera->posZ,
        0.0f                         , 0.0f                         , 0.0f                         ,  1.0f
    );


    //Set new uniform viewMatrix value,
    //set transpose to GL_TRUE because we use
    //a row major matrix storage mode
    glUniformMatrix4fv(camera->viewMatrixLocation, 1, GL_TRUE, getMatrixPointer(viewMatrix));


    return GL_TRUE;

}

GLboolean updateProjectionMatrix(CameraData* camera, GLfloat l, GLfloat t, GLfloat r, GLfloat b, GLfloat n, GLfloat f) {

    //  Construct projection matrix and update
    //  uniform variable
    

    MATRIX_FLOAT* projectionMatrix = createProjectionMatrix(l, t, r, b, n, f);


    glUniformMatrix4fv(camera->projectionMatrixLocation, 1, GL_TRUE, getMatrixPointer(projectionMatrix));


    free(projectionMatrix);


    return GL_TRUE;
}

GLboolean updateProjectionMatrixFieldOfView(CameraData* camera, GLfloat fov, GLfloat aspect, GLfloat n, GLfloat f) {
    
    MATRIX_FLOAT* projectionMatrix = createProjectionMatrixFieldOfView(fov, aspect, n, f);


    glUniformMatrix4fv(camera->projectionMatrixLocation, 1, GL_TRUE, getMatrixPointer(projectionMatrix));


    free(projectionMatrix);


    return GL_TRUE;

}

GLboolean updateViewMatrixUniformLocation(CameraData* camera, ProgramData* program, char* viewMatrixName) {

    //  Just update view matrix location
    //  through glGetUniformLocation function


    camera->viewMatrixLocation = glGetUniformLocation(program->programID, viewMatrixName);


    return GL_TRUE;

}

GLboolean updateProjectionMatrixUniformLocation(CameraData* camera, ProgramData* program, char* projectionMatrixName) {

    camera->projectionMatrixLocation = glGetUniformLocation(program->programID, projectionMatrixName);


    return GL_TRUE;

}
