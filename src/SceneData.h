#pragma once

//OpenGL includes
#include "OpenGLIncludes.h"

//Windows includes
#include "WindowsIncludes.h"

//Other includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "ObjectData.h"
#include "Camera.h"


//Set up temporary default
//frustum parameters
#define NEAR 1.0f
#define FAR 10000.0f
#define LEFT -1.0f
#define RIGHT 1.0f
#define BOTTOM -4.5f / 8.0f
#define TOP 4.5f / 8.0f


typedef struct {

    ObjectData objectData;
    struct ObjectListNode* next;

} ObjectListNode;

typedef struct {

    ObjectListNode* head;
    GLuint objectListSize;

} ObjectList;

//Description of the SceneData structure
typedef struct {

    GLuint modelMatrixLocation;
    GLuint numOfVertices;
    GLuint numOfIndices;
    ObjectData* objects;
    CameraData* camera;
    ObjectList objectList;
    GLuint arrayBufferID;
    GLuint elementArrayBufferID;
    GLuint vertexArrayObjectID;

} SceneData;




//Functions for controlling
//the state of scenes
//--------------------------------
GLvoid attachObject(SceneData* scene, ObjectData* object);
GLvoid attachCamera(SceneData* scene, CameraData* camera);
GLvoid prepareRenderData(SceneData* scene);
GLvoid clearRenderData(SceneData* scene);
GLvoid renderScene(SceneData* scene);
GLvoid copyAndShiftIndices(GLuint number, GLuint shift, GLuint* to, GLuint* from);
GLuint getNumberOfObjects(SceneData* scene);
GLvoid updateModelMatrix(SceneData* scene, ObjectData* object);
GLvoid udpateModelMatrixLocation(SceneData* scene, ProgramData* program, char* name); 
//--------------------------------
//Functions for working
//with lists of objects
//--------------------------------
GLvoid createObjectList(ObjectList* list);
GLvoid clearObjectList(ObjectList* list);
GLvoid objectListPushBack(ObjectList* list, ObjectData* object);
GLvoid copyObjectData(ObjectData* to, ObjectData* from);
GLvoid copyVerticesData(GLuint vertices, VertexData* to, VertexData* from);
GLvoid copyIndicesData(GLuint indices, GLuint* to, GLuint* from);
//--------------------------------
