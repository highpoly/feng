#pragma once

//OpenGL includes
#include "OpenGLIncludes.h"

//Windows includes
#include "WindowsIncludes.h"

//Other includes
#include <stdio.h>
#include <tchar.h>
#include <stdlib.h>
#include <string.h>


#define WINDOW_POS_X 0
#define WINDOW_POS_Y 0
#define WINDOW_WIDTH 1366
#define WINDOW_HEIGHT 768


static TCHAR displayWindowName[] = _T("OpenGL Application");
static TCHAR displayWindowClassName[] = _T("DisplayWindowClass");




int RegisterDisplayWindowClass(HINSTANCE hInstance);
HWND CreateDisplayWindow(HINSTANCE hInstance);
LRESULT CALLBACK DisplayWindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void MessageProcLoop();
void RedisplayWindow(HWND hWnd);
