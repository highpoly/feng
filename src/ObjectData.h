#pragma once

//OpenGL includes
#include "OpenGLIncludes.h"

//Windows includes
#include "WindowsIncludes.h"

//Other includes
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Math.h"




typedef struct {

    GLfloat xyzw[4];

} VertexData;

typedef struct {

    VertexData vertex;
    struct VertexListNode* next;

} VertexListNode;

typedef struct {

    VertexListNode* head;
    GLuint vertexListSize;

} VertexList;

typedef struct {

    GLuint numOfVertices;
    GLuint numOfIndices;
    VertexData* vertices;
    GLuint* indices;
    GLuint objectID;
    GLfloat translationX;
    GLfloat translationY;
    GLfloat translationZ;
    GLfloat rotationX;
    GLfloat rotationY;
    GLfloat rotationZ;
    GLfloat scaleX;
    GLfloat scaleY;
    GLfloat scaleZ;

} ObjectData;




//Functions for controlling 
//vertex list state
//--------------------------------
GLvoid createVertexList(VertexList* list);
GLvoid clearVertexList(VertexList* list);
GLvoid vertexListPushBack(VertexList* list, VertexData vertex);
GLvoid copyVertexData(VertexData* to, VertexData* from);
//--------------------------------
//Translation, scaling, rotation
//--------------------------------
GLvoid rotateObject(ObjectData* object, GLfloat x, GLfloat y, GLfloat Z);
GLvoid translateObject(ObjectData* object, GLfloat x, GLfloat y, GLfloat z);
GLvoid scaleObject(ObjectData* object, GLfloat x, GLfloat y, GLfloat z);
//--------------------------------
