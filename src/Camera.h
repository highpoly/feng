#pragma once

//OpenGL includes
#include "OpenGLIncludes.h"

//Windows includes
#include "WindowsIncludes.h"

//Other includes
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "ShaderLoading.h"


//There is a desription of CameraData structure
typedef struct {

    GLfloat roll; 
    GLfloat yaw;
    GLfloat pitch;
    GLfloat posX;
    GLfloat posY;
    GLfloat posZ;
    GLuint viewMatrixLocation;
    GLuint projectionMatrixLocation;

} CameraData;




GLboolean initCamera(CameraData* camera, ProgramData* program, char* viewMatrixName, char* projectionMatrixName);
GLboolean rotateCamera(CameraData* camera, GLfloat roll, GLfloat yaw, GLfloat pitch);
GLboolean translateCamera(CameraData* camera, GLfloat x, GLfloat y, GLfloat z);
GLboolean updateViewMatrix(CameraData* camera);
GLboolean updateProjectionMatrix(CameraData* camera, GLfloat l, GLfloat t, GLfloat r, GLfloat b, GLfloat n, GLfloat f);
GLboolean updateProjectionMatrixFieldOfView(CameraData* camera, GLfloat fov, GLfloat aspect, GLfloat n, GLfloat f);
GLboolean updateViewMatrixUniformLocation(CameraData* camera, ProgramData* program, char* viewMatrixName);
GLboolean updateProjectionMatrixUniformLocation(CameraData* camera, ProgramData* program, char* projectionMatrixName);
