#include "RenderingContext.h"
#include "ShaderLoading.h"
#include "RenderCore.h"




void CreateRenderingContext(HWND hWnd) {


    HDC hdc = GetDC(hWnd);
    PixelFormatInitialization(hdc);
    HGLRC hglrc = wglCreateContext(hdc);
    wglMakeCurrent(hdc, hglrc);
    

    //GLEW initialization and error processing
    if (glewInit()) {
        fputs("Error : GLEW initialization failure!", stderr);
        exit(EXIT_FAILURE);
    }

}

void DeleteRenderingContext(HWND hWnd) {

    HGLRC hglrc = wglGetCurrentContext();
    HDC hdc = wglGetCurrentDC();


    wglMakeCurrent(NULL, NULL);


    ReleaseDC(hWnd, hdc);
    wglDeleteContext(hglrc);

}

void PixelFormatInitialization(HDC hdc) {

    PIXELFORMATDESCRIPTOR pfd = {
        sizeof(PIXELFORMATDESCRIPTOR),
        4, 
        PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER, PFD_TYPE_RGBA,
        24,
        0, 0, 0, 0, 0,
        0,
        0,
        0,
        0, 0, 0, 0,
        32,
        0,
        0,
        PFD_MAIN_PLANE,
        0, 
        0, 0, 0
    };


    int pixelFormat = ChoosePixelFormat(hdc, &pfd);


    SetPixelFormat(hdc, pixelFormat, &pfd);

}

void DisplayLoop(HWND hWnd) {

    HDC hdc = GetDC(hWnd);

        
        startRendering();


        //Run main display loop
        BOOL continueDrawing = TRUE;
        do {
            MSG msg;
            while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE)) {
                if (GetMessage(&msg, 0, 0, 0)) {
                    TranslateMessage(&msg);
                    DispatchMessage(&msg);
                } else {
                    continueDrawing = FALSE;
                }
                renderMainScene();
                SwapBuffers(hdc);
            }
        } while (continueDrawing);


        finishRendering();


    ReleaseDC(hWnd, hdc);

}
