#include "DisplayWindow.h"
#include "RenderCore.h"
#include "RenderingContext.h"




int RegisterDisplayWindowClass(HINSTANCE hInstance) {
    
    WNDCLASS wndclass;
    
    
    wndclass.style = CS_HREDRAW | CS_VREDRAW;
    wndclass.lpfnWndProc = DisplayWindowProc;
    wndclass.cbClsExtra = 0;
    wndclass.cbWndExtra = 0;
    wndclass.hInstance = hInstance;
    wndclass.hIcon = NULL;
    wndclass.hCursor = NULL;
    wndclass.hbrBackground = NULL;
    wndclass.lpszMenuName = displayWindowName;
    wndclass.lpszClassName = displayWindowClassName;


    return RegisterClass(&wndclass);

}

HWND CreateDisplayWindow(HINSTANCE hInstance) {
    
    return CreateWindow(
        displayWindowClassName, displayWindowName,
        WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
        WINDOW_POS_X, WINDOW_POS_Y, WINDOW_WIDTH, WINDOW_HEIGHT,
        NULL, NULL, hInstance, NULL
    );

}

LRESULT CALLBACK DisplayWindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
    
    switch (message) {
        case WM_CREATE:
            CreateRenderingContext(hWnd);
            break;
        case WM_DESTROY:
            DeleteRenderingContext(hWnd);
            PostQuitMessage(0);
            break;
        default:
            return DefWindowProc(hWnd, message, wParam, lParam);
    }


    return 0;

}

void RedisplayWindow(HWND hWnd) {
    
    ShowWindow(hWnd, SW_SHOW);
    UpdateWindow(hWnd);

}
