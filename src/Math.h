/*
    This module contains a set of 
    vector and matrix operations.
    There are used row-major matrix storage mode
    and 0-indexation in vectors in matrices.
    Dot, cross, and mixed product operation
    require orthonormalized basis
*/


#pragma once 

//OpenGL includes
#include "OpenGLIncludes.h"

//Windows includes
#include "WindowsIncludes.h"

//Other includes
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>


//Define constants
#define PI 3.1415926535

//Generic macro definitions
#define set(X, Y) _Generic((X), MATRIX_FLOAT*: setMatrix(X, Y), VECTOR_FLOAT*: setVector(X, Y))
#define set(X, Y, Z) vectorSetAt(X, Y, Z)
#define set(X, Y, Z, W) matrixSetAt(X, Y, Z, W)
#define get(X, Y) vectorGetAt(X, Y)
#define get(X, Y, Z) matrixGetAt(X, Y, Z)


typedef struct {

    //isResult - means, that current variable
    //is the result of some operation and must
    //be removed after setting up its data to
    //other variable
    

    GLboolean isResult;
    GLfloat* coordinates;
    GLint numberOfCoordinates;//The number of coordinates
    
} VECTOR_FLOAT;

typedef struct {

    //isResult - means, that current variable
    //is the result of some operation and must 
    //be removed after setting up its data to
    //other variable
    
    
    GLboolean isResult;
    GLfloat* elements;
    GLint rows;
    GLint columns;

} MATRIX_FLOAT;



//Basic math functions
//--------------------------------
GLint minimalOf(GLint x, GLint y);
GLint maximalOf(GLint x, GLint y);
GLfloat toRadians(GLfloat angle);
GLboolean increaseAngle(GLfloat* angle, GLfloat delta);
//--------------------------------
//Functions used for setting up new values
//--------------------------------
GLboolean setVector(VECTOR_FLOAT* to, VECTOR_FLOAT* from);
GLboolean setMatrix(MATRIX_FLOAT* to, MATRIX_FLOAT* from);
//--------------------------------
//Functions used to get and set elements and coordinates
//--------------------------------
GLfloat vectorGetAt(VECTOR_FLOAT* vector, GLint numOfCoordinate);
GLfloat matrixGetAt(MATRIX_FLOAT* matrix, GLint row, GLint column);
GLboolean vectorSetAt(VECTOR_FLOAT* vector, GLint numOfCoordinate, GLfloat newValue);
GLboolean matrixSetAt(MATRIX_FLOAT* matrix, GLint rowNumber, GLint columnNumber, GLfloat newValue);
GLboolean setRow(MATRIX_FLOAT* matrix, GLint rowNumber, VECTOR_FLOAT* vector);
GLboolean setColumn(MATRIX_FLOAT* matrix, GLint columnNumber, VECTOR_FLOAT* vector);
VECTOR_FLOAT* getRow(MATRIX_FLOAT* matrix, GLint rowNumber);
VECTOR_FLOAT* getColumn(MATRIX_FLOAT* matrix, GLint columnNumber);
//--------------------------------
//Functions used for creation new elements
//--------------------------------
VECTOR_FLOAT* createVector(GLint size, GLboolean isResult, GLint argCnt, ...);
MATRIX_FLOAT* createMatrix(GLint rows, GLint columns, GLboolean isResult, GLint rowsArg, GLint columnsArg, ...);
//--------------------------------
//Operations
//--------------------------------
VECTOR_FLOAT* crossProduct(VECTOR_FLOAT* vector1, VECTOR_FLOAT* vector2);
VECTOR_FLOAT* multiplyVectorOnMatrix(VECTOR_FLOAT* vector, MATRIX_FLOAT* matrix);
VECTOR_FLOAT* multiplyMatrixOnVector(MATRIX_FLOAT* matrix, VECTOR_FLOAT* vector);
VECTOR_FLOAT* multiplyVectorOnNumber(VECTOR_FLOAT* vector, GLfloat value);
VECTOR_FLOAT* multiplyNumberOnVector(GLfloat value, VECTOR_FLOAT* vector);
VECTOR_FLOAT* rotateVector(VECTOR_FLOAT* vector, GLfloat aboutX, GLfloat aboutY, GLfloat aboutZ);
MATRIX_FLOAT* createProjectionMatrix(GLfloat l, GLfloat t, GLfloat r, GLfloat b, GLfloat n, GLfloat f);
MATRIX_FLOAT* createProjectionMatrixFieldOfView(GLfloat fov, GLfloat aspect, GLfloat n, GLfloat f);
MATRIX_FLOAT* createOrthographicProjectionMatrix(GLfloat l, GLfloat t, GLfloat r, GLfloat b, GLfloat n, GLfloat f);
MATRIX_FLOAT* createOrthographicProjectionMatrixFieldOfView(GLfloat fov, GLfloat aspect, GLfloat n, GLfloat f);
MATRIX_FLOAT* multiplyMatrices(MATRIX_FLOAT* matrix1, MATRIX_FLOAT* matrix2);
MATRIX_FLOAT* multiplyMatrixOnNumber(MATRIX_FLOAT* matrix, GLfloat value);
MATRIX_FLOAT* multiplyNumberOnMatrix(GLfloat value, MATRIX_FLOAT* matrix);
MATRIX_FLOAT* inverseMatrix(MATRIX_FLOAT* matrix);
MATRIX_FLOAT* identityMatrix(GLint dimension);
MATRIX_FLOAT* transposeMatrix(MATRIX_FLOAT* matrix);
GLfloat dotProduct(VECTOR_FLOAT* vector1, VECTOR_FLOAT* vector2);
GLfloat length(VECTOR_FLOAT* vector);
GLfloat determinant(MATRIX_FLOAT* matrix);
GLfloat mixedProduct(GLint dimension, ...);
//----------------------------------------
//Service functions
//----------------------------------------
GLfloat* getMatrixPointer(MATRIX_FLOAT* matrix);
GLfloat* getVectorPointer(VECTOR_FLOAT* vector);
//----------------------------------------
