//OpenGL includes
#include <gl\glew.h>
#include <gl\gl.h>
#include <gl\glu.h>

//Windows includes
#include <windows.h>
#include <wingdi.h>

//Other includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


//Default error log file name
#define ERR_LOG_FILE_NAME "c:\\tasks\\repo\\glapp1\\tmp\\errlog.txt"


static FILE* errlog = NULL; //  Default error log output file

static unsigned long errorNumber = 0;//  Current error number(0 if logging is not initialized)




//Error logging initialization functions
//--------------------------------
GLvoid initErrorTracking();
GLvoid finishErrorTracking();
//--------------------------------
//Error logging state control functions
//--------------------------------
GLboolean logData(char* dataString);
GLboolean logError(char* errorString);
GLboolean errorTrackingIsActive();
GLvoid incErrorNumber();
//--------------------------------
//Shader errors catching functions
//--------------------------------
GLboolean checkShaderID(GLuint shaderID);
GLboolean checkShaderCompilationStatus(GLuint shaderID);
GLboolean checkShaderSource(GLuint shaderID);
GLboolean checkShaderDeleteStatus(GLuint shaderID);
GLboolean checkShaderDetachStatus(GLuint shaderID);
GLboolean checkShaderAttachStatus(GLuint shaderID);
//--------------------------------
//Program errors catching functions
//--------------------------------
GLboolean checkProgramID(GLuint programID);
GLboolean checkProgramLinkingStatus(GLuint programID);
GLboolean checkProgramDeleteStatus(GLuint programID);
//--------------------------------
