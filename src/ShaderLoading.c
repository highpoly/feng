#include "ShaderLoading.h"
#include "ErrorTracking.h"




GLboolean deleteProgram(ProgramData* program) {

    if (checkProgramID(program->programID)) {
        return GL_FALSE;
    }


    //  Call detachShaders
    detachShaders(program);


    glDeleteProgram(program->programID);
    

    return checkProgramDeleteStatus(program->programID);

}

GLboolean detachShaders(ProgramData* program) {
    
    if (!checkProgramID(program->programID)) {
        return GL_FALSE;
    }


    //  Get the list of attached shaders
    GLint numOfShaders;
    GLuint* attachedShaders;


    glGetProgramiv(program->programID, GL_ATTACHED_SHADERS, &numOfShaders);
    attachedShaders = calloc(numOfShaders, sizeof(GLuint));
    glGetAttachedShaders(program->programID, numOfShaders, NULL, attachedShaders);


    //  Next watch the list and detach each shader from the program
    for (int i = 0; i < numOfShaders; i++) {
        detachShader(program, attachedShaders[i]);
        deleteShader(attachedShaders[i]);
    }


    free(attachedShaders);


    return GL_TRUE;

}

GLboolean attachShaders(ProgramData* program, int numOfShaders, ShaderData* shaders) {

    if (!checkProgramID(program->programID)) {
        return GL_FALSE;
    }


    for (int i = 0; i < numOfShaders; i++) {
        if (!createShader(&shaders[i]) || !attachShader(program, shaders[i].shaderID)) {
            deleteProgram(program);
            return GL_FALSE;
        }
    }


    return GL_TRUE;

}

GLboolean createProgram(ProgramData* program, GLint numOfShaders, ShaderData* shaders) {

    program->programID = glCreateProgram();
    

    //  Create and attach shaders to the program
    if (!attachShaders(program, numOfShaders, shaders)) {
        return GL_FALSE;
    }
    

    //  Link program and check program linking result
    return linkProgram(program); 
    
}

GLboolean linkProgram(ProgramData* program) {

    if (!checkProgramID(program->programID)) {
        return GL_FALSE;
    }

    
    glLinkProgram(program->programID);


    return checkProgramLinkingStatus(program->programID);

}

GLboolean createShader(ShaderData* shader) {
    
    //  First create shader object
    shader->shaderID = glCreateShader(shader->shaderType);

    
    return (loadShaderSource(shader) && compileShader(shader));

}

GLboolean loadShaderSource(ShaderData* shader) {

    if (!checkShaderID(shader->shaderID)) {
        return GL_FALSE;
    }


    //  Load shader source from file
    char* shaderSource;
    
    
    FILE* file = fopen(shader->fileName, "rb");
    
        long fileSize = sizeOfFile(file);
        shaderSource = calloc(fileSize + 1, sizeof(char));
        shaderSource[fileSize] = '\0';
        fread(shaderSource, sizeof(char), fileSize, file);
        glShaderSource(shader->shaderID, 1, &shaderSource, NULL);
        free(shaderSource);

    fclose(file);


    return checkShaderSource(shader->shaderID);

}

GLboolean compileShader(ShaderData* shader) {

    if (!checkShaderID(shader->shaderID)) {
        return GL_FALSE;
    }


    glCompileShader(shader->shaderID);


    //  Catch and process compilation errors
    return checkShaderCompilationStatus(shader->shaderID);

}

GLboolean deleteShader(GLuint shaderID) {

    if (!checkShaderID(shaderID)) {
        return GL_FALSE;
    }


    glDeleteShader(shaderID);


    return checkShaderDeleteStatus(shaderID);

}

GLboolean detachShader(ProgramData* program, GLuint shaderID) {
    
    if (!checkProgramID(program->programID) || !checkShaderID(shaderID)) {
        return GL_FALSE;
    }


    glDetachShader(program->programID, shaderID);


    return checkShaderDetachStatus(shaderID);

}

GLboolean attachShader(ProgramData* program, GLuint shaderID) {
    
    if (!checkProgramID(program->programID) || !checkShaderID(shaderID)) {
        return GL_FALSE;
    }


    glAttachShader(program->programID, shaderID);


    return checkShaderAttachStatus(shaderID);

}

long sizeOfFile(FILE* file) {

    long fileSize;


    fseek(file, 0, SEEK_END);
    fileSize = ftell(file);
    fseek(file, 0, SEEK_SET);
    

    return fileSize;

}
