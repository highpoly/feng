#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gl\glew.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include "DisplayWindow.h"





int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {  

    if (!RegisterDisplayWindowClass(hInstance)) {
        MessageBox(NULL, _T("Display window class creation failure!"), _T("Error!"), NULL);
        exit(EXIT_FAILURE);
    }


    HWND hWndDisplayWindow;
    hWndDisplayWindow = CreateDisplayWindow(hInstance);
    if (!hWndDisplayWindow) {
        MessageBox(NULL, _T("Display window creation failure!"), _T("Error!"), NULL);
        exit(EXIT_FAILURE);
    }


    MessageBox(NULL, _T("Display window creation successful."), _T("Done!"), NULL);


    RedisplayWindow(hWndDisplayWindow);


    DisplayLoop(hWndDisplayWindow);


    return 0;

}
