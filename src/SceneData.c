#include "SceneData.h"




GLvoid attachObject(SceneData* scene, ObjectData* object) {  

    objectListPushBack(&(scene->objectList), object);
    scene->numOfVertices += object->numOfVertices;
    scene->numOfIndices += object->numOfIndices;

}

GLvoid attachCamera(SceneData* scene, CameraData* camera) {
    
    scene->camera = camera;

}

GLvoid renderScene(SceneData* scene) {

    GLuint n = getNumberOfObjects(scene);


    GLuint indicesToSkip = 0;
    for (int i = 0; i < n; i++) {
        updateModelMatrix(scene, &(scene->objects[i]));
        glDrawElements(
            GL_TRIANGLES, scene->objects[i].numOfIndices,
            GL_UNSIGNED_INT, (const void*)(indicesToSkip * sizeof(GLuint))
        );
        indicesToSkip += scene->objects[i].numOfIndices;
    }

}

GLvoid createObjectList(ObjectList* list) {

    list->head = NULL;
    list->objectListSize = 0;

}

GLvoid clearObjectList(ObjectList* list) {

    ObjectListNode* travelPtr = list->head;
    

    while (travelPtr != NULL) {
        ObjectListNode* tempPtr = travelPtr;
        travelPtr = travelPtr->next;
        free(travelPtr);
        list->objectListSize--;
    }

}

GLvoid objectListPushBack(ObjectList* list, ObjectData* object) {
    
    //First create new object
    ObjectListNode* newObject = malloc(sizeof(ObjectListNode));
    

    //Initialize all values
    copyObjectData(&(newObject->objectData), object);
    newObject->next = NULL;


    //Update head pointer and objects counter
    if (list->head != NULL) {
        list->head->next = newObject;
    }
    list->head = newObject;
    list->objectListSize++;

}

GLvoid copyObjectData(ObjectData* to, ObjectData* from) {
    
    to->numOfVertices = from->numOfVertices;
    to->numOfIndices = from->numOfIndices;
    to->objectID = from->objectID;
    to->translationX = from->translationX;
    to->translationY = from->translationY;
    to->translationZ = from->translationZ;
    to->scaleX = from->scaleX;
    to->scaleY = from->scaleY;
    to->scaleZ = from->scaleZ;
    to->rotationX = from->rotationX;
    to->rotationY = from->rotationY;
    to->rotationZ = from->rotationZ;
    to->vertices = calloc(to->numOfVertices, sizeof(VertexData));
    to->indices = calloc(to->numOfIndices, sizeof(GLuint));
    copyVerticesData(to->numOfVertices, to->vertices, from->vertices);
    copyIndicesData(to->numOfIndices, to->indices, from->indices);

}

GLvoid copyIndicesData(GLuint indices, GLuint* to, GLuint* from) {
    
    //Just copy all indices from 'from' in 'to' array
    

    for (int i = 0; i < indices; i++) {
        to[i] = from[i];
    }

}

GLvoid copyVerticesData(GLuint vertices, VertexData* to, VertexData* from) {
    
    //Just copy all vertices from 'from' in 'to'
    

    for (int i = 0; i < vertices; i++) {
        copyVertexData(&to[i], &from[i]);
    }

}

GLuint getNumberOfObjects(SceneData* scene) {
    
    return scene->objectList.objectListSize;

}

GLvoid prepareRenderData(SceneData* scene) {
    
    //First update view and projection matrices
    updateViewMatrix(scene->camera);
    //updateProjectionMatrix(scene->camera, LEFT, TOP, RIGHT, BOTTOM, NEAR, FAR);
    updateProjectionMatrixFieldOfView(scene->camera, toRadians(45.0f), 16.0f / 9.0f, 1.0f, 10000.0f);
    

    //Create and bind vertex array object
    glGenVertexArrays(1, &(scene->vertexArrayObjectID));
    glBindVertexArray(scene->vertexArrayObjectID);


    //Next load vertices and surface data
    //from objects list
    glGenBuffers(1, &(scene->arrayBufferID));
    glBindBuffer(GL_ARRAY_BUFFER, scene->arrayBufferID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(VertexData) * scene->numOfVertices, NULL, GL_STATIC_DRAW);

    
    glGenBuffers(1, &(scene->elementArrayBufferID));
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, scene->elementArrayBufferID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * scene->numOfIndices, NULL, GL_STATIC_DRAW);

    
    //Next set up vertex attrib pointer
    glVertexAttribPointer(
        0, 4, GL_FLOAT, GL_FALSE, 
        sizeof(VertexData), 0
    );
    glEnableVertexAttribArray(0);


    GLuint n = getNumberOfObjects(scene);
    //Load objects from objects list to array
    scene->objects = calloc(getNumberOfObjects(scene), sizeof(ObjectData));
    ObjectListNode* travelPtr = scene->objectList.head;
    for (int i = 0; i < n; i++) {
        copyObjectData(&(scene->objects[i]), &(travelPtr->objectData));
        travelPtr = travelPtr->next;
    }


    GLuint loadedVertices = 0;
    GLuint loadedIndices = 0;
    for (int i = 0; i < n; i++) {
        //Loading of indices to 
        //draw surface of the object
        GLuint* tempIndices = calloc(scene->objects[i].numOfIndices, sizeof(GLuint));
        copyAndShiftIndices(
            scene->objects[i].numOfIndices, loadedIndices,
            tempIndices, scene->objects[i].indices
        );
        glBufferSubData(
            GL_ELEMENT_ARRAY_BUFFER, loadedIndices * sizeof(GLuint), 
            scene->objects[i].numOfIndices * sizeof(GLuint), (const void*)tempIndices
        );
        free(tempIndices);
        //Loading of the vertices data
        //into the vertex buffer 
        glBufferSubData(
            GL_ARRAY_BUFFER, loadedVertices * sizeof(VertexData),
            scene->objects[i].numOfVertices * sizeof(VertexData), (const void*)scene->objects[i].vertices
        );
        loadedVertices += scene->objects[i].numOfVertices;
        loadedIndices += scene->objects[i].numOfIndices;
    }
    
}

GLvoid copyAndShiftIndices(GLuint number, GLuint shift, GLuint* to, GLuint* from) {
    
    for (int i = 0; i < number; i++) {
        to[i] = shift + from[i];
    }

}

GLvoid clearRenderData(SceneData* scene) {

    //Just delete buffer and array objects
    glDeleteBuffers(1, &(scene->arrayBufferID));
    glDeleteBuffers(1, &(scene->elementArrayBufferID));
    glDeleteVertexArrays(1, &(scene->vertexArrayObjectID));

}

GLvoid updateModelMatrix(SceneData* scene, ObjectData* object) {

    //First create matrices
    MATRIX_FLOAT* translationMatrix = createMatrix(
        4, 4, GL_TRUE, 4, 4,
        1.0f, 0.0f, 0.0f, object->translationX,
        0.0f, 1.0f, 0.0f, object->translationY,
        0.0f, 0.0f, 1.0f, object->translationZ,
        0.0f, 0.0f, 0.0f, 1.0f
    );


    GLfloat axisX = toRadians(object->rotationX);
    GLfloat axisY = toRadians(object->rotationY);
    GLfloat axisZ = toRadians(object->rotationZ);
    MATRIX_FLOAT* rotationMatrix = createMatrix(
        4, 4, GL_TRUE, 4, 4,
        //Construct axis X rotation matrix
        1.0f,  0.0f       ,  0.0f       ,  0.0f,
        0.0f,  cosf(axisX), -sinf(axisX),  0.0f,
        0.0f,  sinf(axisX),  cosf(axisX),  0.0f,
        0.0f,  0.0f       ,  0.0f       ,  1.0f
    );
    setMatrix(
        rotationMatrix,
        multiplyMatrices(
            rotationMatrix,
            createMatrix(
                4, 4, GL_TRUE, 4, 4,
                //Axis Y rotation matrix
                cosf(axisY),  0.0f, -sinf(axisY),  0.0f,
                0.0f       ,  1.0f,  0.0f       ,  0.0f,
                sinf(axisY),  0.0f,  cosf(axisY),  0.0f,
                0.0f       ,  0.0f,  0.0f       ,  1.0f
            )
        )
    );
    setMatrix(
        rotationMatrix, 
        multiplyMatrices(
            rotationMatrix, 
            createMatrix(
                4, 4, GL_TRUE, 4, 4,
                //Consctruct axis z rotation matrix
                cosf(axisZ), -sinf(axisZ),  0.0f,  0.0f,
                sinf(axisZ),  cosf(axisZ),  0.0f,  0.0f,
                0.0f       ,  0.0f       ,  1.0f,  0.0f,
                0.0f       ,  0.0f       ,  0.0f,  1.0f
            )
        )
    );


    MATRIX_FLOAT* scaleMatrix = createMatrix(
        4, 4, GL_TRUE, 4, 4,
        object->scaleX, 0.0f          , 0.0f          , 0.0f,
        0.0f          , object->scaleY, 0.0f          , 0.0f,
        0.0f          , 0.0f          , object->scaleZ, 0.0f,
        0.0f          , 0.0f          , 0.0f          , 1.0f
    );


    setMatrix(translationMatrix, multiplyMatrices(translationMatrix, rotationMatrix));
    setMatrix(translationMatrix, multiplyMatrices(translationMatrix, scaleMatrix));


    glUniformMatrix4fv(
        scene->modelMatrixLocation, 1, GL_TRUE,
        getMatrixPointer(translationMatrix)
    );

}

GLvoid updateModelMatrixLocation(SceneData*scene, ProgramData* program, char* name) {

    scene->modelMatrixLocation = glGetUniformLocation(program->programID, name);

}
