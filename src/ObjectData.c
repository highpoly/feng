#include "ObjectData.h"




GLvoid createVertexList(VertexList* list) {
   
    list->head = NULL;
    list->vertexListSize = 0;

}

GLvoid clearVertexList(VertexList* list) {

    VertexListNode* travelPtr = list->head;
    while (travelPtr != NULL) {
        VertexListNode* temp = travelPtr;
        travelPtr = travelPtr->next;
        free(temp);
        list->vertexListSize--;
    }

}

GLvoid vertexListPushBack(VertexList* list, VertexData vertex) {

    //First create new node
    VertexListNode* newVertex = malloc(sizeof(VertexListNode));
    
    
    //Fill vertex data
    copyVertexData(&(newVertex->vertex), &vertex);
    newVertex->next = NULL;


    //Update list->head pointer and vertex counter
    if (list->head != NULL) {
        list->head->next = newVertex;
    }
    list->head = newVertex;
    list->vertexListSize++;

}

GLvoid copyVertexData(VertexData* to, VertexData* from) {
    
    for (int i = 0; i < 4; i++) {
        to->xyzw[i] = from->xyzw[i];
    }

}

GLvoid rotateObject(ObjectData* object, GLfloat x, GLfloat y, GLfloat z) {

    increaseAngle(&(object->rotationX), x);
    increaseAngle(&(object->rotationY), y);
    increaseAngle(&(object->rotationZ), z);

}

GLvoid translateObject(ObjectData* object, GLfloat x, GLfloat y, GLfloat z) {
    
    object->translationX += x;
    object->translationY += y;
    object->translationZ += z;

}

GLvoid scaleObject(ObjectData* object, GLfloat x, GLfloat y, GLfloat z) {
    
    object->scaleX += x;
    object->scaleY += y;
    object->scaleZ += z;

}
