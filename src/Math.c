#include "Math.h"




GLint minimalOf(GLint x, GLint y) {
    
    return x < y ? x : y;

}

GLint maximalOf(GLint x, GLint y) {
    
    return x > y ? x : y;

}

GLfloat toRadians(GLfloat angle) {
    
    return (PI / 180.0f) * angle;

}

GLboolean increaseAngle(GLfloat* angle, GLfloat delta) {
    
    *angle += delta;

    if (fabs(*angle) > 360.0f) {
        *angle -= 360.0f * ((GLint)(*angle) / 360);
    }

}

VECTOR_FLOAT* createVector(GLint size, GLboolean isResult, GLint argCnt, ...) {
    
    //  Get the number of coordinates in 'size' variable
    //  and check arguments list
    

    //  First set up va_list variable
    va_list args;
    va_start(args, argCnt);


    //  Allocate the memory to store coordinates
    //  and initialize array elements as null
    VECTOR_FLOAT* newVector = malloc(sizeof(VECTOR_FLOAT));
    newVector->numberOfCoordinates = size;
    newVector->coordinates = calloc(size, sizeof(GLfloat));
    memset(newVector->coordinates, 0, size * sizeof(GLfloat));


    GLint n = minimalOf(argCnt, size);
    for (int i = 0; i < n; i++) {
        vectorSetAt(newVector, i, va_arg(args, GLdouble));
    }


    //  Set  up isResult variable to isResult parameter and
    //  finish va_arg variable usage
    newVector->isResult = isResult;
    va_end(args);


    return newVector;

}

MATRIX_FLOAT* createMatrix(GLint rows, GLint columns, GLboolean isResult, GLint rowsArg, GLint columnsArg, ...) {

    //  Set up va_list variable
    va_list args;
    va_start(args, columnsArg);


    //  Initialize matrix structure members
    MATRIX_FLOAT* newMatrix = malloc(sizeof(MATRIX_FLOAT));
    newMatrix->rows = rows;
    newMatrix->columns = columns;
    newMatrix->elements = calloc(rows * columns, sizeof(GLfloat));
    memset(newMatrix->elements, 0.0f, rows * columns * sizeof(GLfloat));


    //  Initialize elements
    for (int i = 0; i < rowsArg; i++) {
        for (int j = 0; j < columnsArg; j++) {
            matrixSetAt(newMatrix, i, j, va_arg(args, GLdouble));
        }
    }
    

    //  Set up isResult variable to isResult parameter value and
    //  finish va_arg variable usage
    newMatrix->isResult = isResult;
    va_end(args);


    return newMatrix;

}

GLboolean setVector(VECTOR_FLOAT* to, VECTOR_FLOAT* from) {

    //  Just copy all elements of 'from' vector in 'to' vector


    //  Set up new values
    to->numberOfCoordinates = from->numberOfCoordinates;
    to->coordinates = calloc(to->numberOfCoordinates, sizeof(GLfloat));
    for (int i = 0; i < to->numberOfCoordinates; i++) {
        to->coordinates[i] = from->coordinates[i];
    }


    //  If 'from' is result of the operation
    //  just free allocated memory for this pointer
    if (from->isResult) {
        //free(from);
    }


    return GL_TRUE;

}

GLboolean setMatrix(MATRIX_FLOAT* to, MATRIX_FLOAT* from) {
    
    //  Just copy all elements of 'from' matrix in 'to' matrix
    

    //  First free the allocated memory in 'to' matrix


    //  Set up new values
    GLint numOfElements = from->rows * from->columns;
    to->rows = from->rows;
    to->columns = from->columns;
    to->elements = calloc(numOfElements, sizeof(GLfloat));
    for (int i = 0; i < numOfElements; i++) {
        to->elements[i] = from->elements[i];
    }


    //  If 'from' is result of operation
    //  just free allocated memory for this matrix pointer
    if (from->isResult) {
        //free(from);
    }


    return GL_TRUE;

}

GLfloat vectorGetAt(VECTOR_FLOAT* vector, GLint numOfCoordinate) {
    
    return vector->coordinates[numOfCoordinate];

}

GLfloat matrixGetAt(MATRIX_FLOAT* matrix, GLint row, GLint column) {
    
    return matrix->elements[matrix->columns * row + column];

}

GLboolean vectorSetAt(VECTOR_FLOAT* vector, GLint numOfCoordinate, GLfloat newValue) {

    vector->coordinates[numOfCoordinate] = newValue;


    return GL_TRUE;

}

GLboolean matrixSetAt(MATRIX_FLOAT* matrix, GLint rowNumber, GLint columnNumber, GLfloat newValue) {
    
    matrix->elements[matrix->columns * rowNumber + columnNumber] = newValue;
    

    return GL_TRUE;

}

GLboolean setRow(MATRIX_FLOAT* matrix, GLint rowNumber, VECTOR_FLOAT* vector) {

    //  Calculate the minimum of columns
    //  and the number of coordinates
    GLint toCopy = minimalOf(matrix->columns, vector->numberOfCoordinates);


    //  Next just copy vector coordinates into the row
    for (int i = 0; i < toCopy; i++) {
        matrixSetAt(matrix, rowNumber, i, vector->coordinates[i]);
    }


    return GL_TRUE;

}

GLboolean setColumn(MATRIX_FLOAT* matrix, GLint columnNumber, VECTOR_FLOAT* vector) {
    
    //  Calculate the minimum of rows
    //  and the number of coordinates
    GLint toCopy = minimalOf(matrix->rows, vector->numberOfCoordinates);


    //  Next just copy vector coordinates into the column
    for (int i = 0; i < toCopy; i++) {
        matrixSetAt(matrix, i, columnNumber, vector->coordinates[i]);
    }


    return GL_TRUE;

}

VECTOR_FLOAT* getRow(MATRIX_FLOAT* matrix, GLint rowNumber) {
    
    //  First create the vector
    //  with coordinates equal to 
    //  elements of row
    VECTOR_FLOAT* rowVector = createVector(matrix->columns, GL_TRUE, 0);
    

    //  Initialize each coordinate
    for (int i = 0; i < matrix->columns; i++) {
        vectorSetAt(rowVector, i, matrixGetAt(matrix, rowNumber, i));
    }


    return rowVector;

}

VECTOR_FLOAT* getColumn(MATRIX_FLOAT* matrix, GLint columnNumber) {

    //  First create the vector
    //  with coordinates equal to
    //  elements of column
    VECTOR_FLOAT* columnVector = createVector(matrix->rows, GL_TRUE, 0);


    //  Initialize coordinates
    for (int i = 0; i < matrix->rows; i++) {
        vectorGetAt(columnVector, i, matrixGetAt(matrix, i, columnNumber));
    }


    return columnVector;

}

MATRIX_FLOAT* identityMatrix(GLint dimension) {
    
    //  Just create identity matrix and return it
    

    MATRIX_FLOAT* resultMatrix = createMatrix(dimension, dimension, GL_TRUE, 0, 0);


    //  Initialize diagonal elements
    for (int i = 0; i < dimension; i++) {
        matrixSetAt(resultMatrix, i, i, 1.0f);
    }


    return resultMatrix;

}

MATRIX_FLOAT* transposeMatrix(MATRIX_FLOAT* matrix) {
    
    //  Create and initialize transposed matrix
    MATRIX_FLOAT* transposed = createMatrix(matrix->rows, matrix->columns, GL_TRUE, 0, 0);
    setMatrix(transposed, matrix);


    //  Transpose matrix
    for (int i = 0; i < transposed->rows; i++) {
        for (int j = i + 1; j < transposed->rows; j++) {
            GLfloat temp = matrixGetAt(transposed, i, j);
            matrixSetAt(transposed, i, j, matrixGetAt(transposed, j, i));
            matrixSetAt(transposed, j, i, temp);
        }
    }


    return transposed;

}

MATRIX_FLOAT* multiplyNumberOnMatrix(GLfloat value, MATRIX_FLOAT* matrix) {

    MATRIX_FLOAT* result = createMatrix(matrix->rows, matrix->columns, GL_TRUE, 0, 0);
    setMatrix(result, matrix);


    for (int i = 0; i < result->rows; i++) {
        for (int j = 0; j < result->columns; j++) {
            matrixSetAt(result, i, j, value * matrixGetAt(result, i, j));
        }
    }


    return result;

}

MATRIX_FLOAT* multiplyMatrixOnNumber(MATRIX_FLOAT* matrix, GLfloat value) { 

    return multiplyNumberOnMatrix(value, matrix);

}

VECTOR_FLOAT* multiplyMatrixOnVector(MATRIX_FLOAT* matrix, VECTOR_FLOAT* vector) {

    MATRIX_FLOAT* multiplyier = createMatrix(vector->numberOfCoordinates, 1, GL_TRUE, 0, 0);
    setColumn(multiplyier, 0, vector);


    //  Use multiplyier pointer as result matrix
    setMatrix(multiplyier, multiplyMatrices(matrix, multiplyier));


    VECTOR_FLOAT* result = getColumn(multiplyier, 0);


    free(multiplyier);


    return result;

}

VECTOR_FLOAT* multiplyVectorOnMatrix(VECTOR_FLOAT* vector, MATRIX_FLOAT* matrix) {
    
    MATRIX_FLOAT* multiplyier = createMatrix(1, vector->numberOfCoordinates, GL_TRUE, 0, 0);
    setRow(multiplyier, 0, vector);

    
    //  Use multiplyier pointer as result matrix
    setMatrix(multiplyier, multiplyMatrices(multiplyier, matrix));


    VECTOR_FLOAT* result = getRow(multiplyier, 0);


    free(multiplyier);


    return result;

}

VECTOR_FLOAT* multiplyVectorOnNumber(VECTOR_FLOAT* vector, GLfloat value) {
    
    VECTOR_FLOAT* result = createVector(vector->numberOfCoordinates, GL_TRUE, 0);
    setVector(result, vector);


    for (int i = 0; i < vector->numberOfCoordinates; i++) {
        vectorSetAt(result, i, vectorGetAt(result, i) * value); 
    }


    return result;

}

VECTOR_FLOAT* multiplyNumberOnVector(GLfloat value, VECTOR_FLOAT* vector) {

    return multiplyVectorOnNumber(vector, value);

}

MATRIX_FLOAT* createProjectionMatrix(GLfloat l, GLfloat t, GLfloat r, GLfloat b, GLfloat n, GLfloat f) {

    return createMatrix(
        4, 4, GL_TRUE, 4, 4,
        2.0f * n / (r - l), 0.0f              , (l + r) / (l - r), 0.0f                    ,
        0.0f              , 2.0f * n / (t - b), (b + t) / (b - t), 0.0f                    ,
        0.0f              , 0.0f              , (f + n) / (f - n), (2.0f * f * n) / (n - f),
        0.0f              , 0.0f              , 1.0f             , 0.0f
    );

}

MATRIX_FLOAT* createProjectionMatrixFieldOfView(GLfloat fov, GLfloat aspect, GLfloat n, GLfloat f) {

    return createMatrix(
        4, 4, GL_TRUE, 4, 4,
        1.0f / (tan(fov / 2.0f) * aspect), 0.0f                  , 0.0f             , 0.0f                    ,
        0.0f                             , 1.0f / tan(fov / 2.0f), 0.0f             , 0.0f                    ,
        0.0f                             , 0.0f                  , (f + n) / (f - n), (2.0f * f * n) / (n - f),
        0.0f                             , 0.0f                  , 1.0f             , 0.0f
    );

}

MATRIX_FLOAT* createOrthographicProjectionMatrix(GLfloat l, GLfloat t, GLfloat r, GLfloat b, GLfloat n, GLfloat f) {
    
    return createMatrix(
        4, 4, GL_TRUE, 4, 4,
        2.0f / (r - l), 0.0f             , 0.0f          , (r + l) / (l - r),
        0.0f          , 2.0f / (t - b)   , 0.0f          , (t + b) / (b - t),
        0.0f          , 0.0f             , 2.0f / (f - n), (n + f) / (n - f),
        0.0f          , 0.0f             , 1.0f          , 0.0f
    );

}

MATRIX_FLOAT* createOrthographicProjectionMatrixFieldOfView(GLfloat fov, GLfloat aspect, GLfloat n, GLfloat f) {
    
    return createMatrix(
        4, 4, GL_TRUE, 4, 4,
        1.0f / (tan(fov / 2.0f) * n * aspect), 0.0f                        , 0.0f          , 0.0f                ,
        0.0f                                 , 1.0f / (tan(fov / 2.0f) * n), 0.0f          , 0.0f                ,
        0.0f                                 , 0.0f                        , 2.0f / (f - n), (n + f) / (n - f),
        0.0f                                 , 0.0f                        , 1.0f          , 0.0f
    );

}

MATRIX_FLOAT* multiplyMatrices(MATRIX_FLOAT* matrix1, MATRIX_FLOAT* matrix2) {
    
    MATRIX_FLOAT* result = createMatrix(matrix1->rows, matrix2->columns, GL_TRUE, 0, 0);
    

    for (int i = 0; i < matrix1->rows; i++) {
        for (int j = 0; j < matrix2->columns; j++) {
            for (int k = 0; k < matrix2->rows; k++) {
                matrixSetAt(result, i, j, matrixGetAt(result, i, j) + matrixGetAt(matrix1, i, k) * matrixGetAt(matrix2, k, j));
            }
        }
    }


    return result;

}

MATRIX_FLOAT* inverseMatrix(MATRIX_FLOAT* matrix) {
    
    //  First check determinant of the matrix
    GLfloat determinantValue = determinant(matrix);
    //return inversed;
    if (determinantValue == 0.0f) {
        return NULL;
    }

    
    MATRIX_FLOAT* inversed = createMatrix(matrix->rows, matrix->rows, GL_TRUE, 0, 0);
    setMatrix(inversed, matrix);
    

    //  Change each element to cofactor
    for (int i = 0; i < inversed->rows; i++) {
        for (int j = 0; j < inversed->rows; j++) {
            matrixSetAt(inversed, i, j, matrixGetAt(inversed, i, j) * powf(-1.0f, i + j));
        }
    }
    

    //  Transpose matrix
    setMatrix(inversed, transposeMatrix(inversed));
    return inversed;

    //  Multiply on 1/determinant
    determinantValue = 1.0f / determinantValue;
    setMatrix(inversed, multiplyNumberOnMatrix(determinantValue, inversed));


    return inversed;

}

VECTOR_FLOAT* crossProduct(VECTOR_FLOAT* vector1, VECTOR_FLOAT* vector2) {

    //  Just calculate cross product

    
    //  Construct determinants
    MATRIX_FLOAT* determinant1 = createMatrix(
        2, 2, GL_TRUE, 2, 2, 
        //  Initialize elements
        vectorGetAt(vector1, 1), vectorGetAt(vector1, 2),
        vectorGetAt(vector2, 1), vectorGetAt(vector2, 2)
    );
    MATRIX_FLOAT* determinant2 = createMatrix(
        2, 2, GL_TRUE, 2, 2,
        vectorGetAt(vector1, 0), vectorGetAt(vector1, 2),
        vectorGetAt(vector2, 0), vectorGetAt(vector2, 2)
    );
    MATRIX_FLOAT* determinant3 = createMatrix(
        2, 2, GL_TRUE, 2, 2,
        vectorGetAt(vector1, 0), vectorGetAt(vector1, 1),
        vectorGetAt(vector2, 0), vectorGetAt(vector2, 1)
    );


    VECTOR_FLOAT* crossProductVector = createVector(
        vector1->numberOfCoordinates, GL_TRUE, 4, 
        -determinant(determinant1),
        determinant(determinant2),
        -determinant(determinant3),
        1.0f//  Set w coordinate to 1
    );


    return crossProductVector;

}

VECTOR_FLOAT* rotateVector(VECTOR_FLOAT* vector, GLfloat aboutX, GLfloat aboutY, GLfloat aboutZ) {
    
    VECTOR_FLOAT* newVector = createVector(
        vector->numberOfCoordinates, GL_TRUE, 3,
        vectorGetAt(vector, 0), vectorGetAt(vector, 1), vectorGetAt(vector, 2)
    );
    GLfloat x, y, z;


    //  Convert degrees to radians;
    aboutX = toRadians(aboutX);
    aboutY = toRadians(aboutY);
    aboutZ = toRadians(aboutZ);


    y = vectorGetAt(newVector, 1);
    z = vectorGetAt(newVector, 2);
    vectorSetAt(newVector, 1, y * cosf(aboutX) - z * sinf(aboutX));
    vectorSetAt(newVector, 2, y * sinf(aboutX) + z * cosf(aboutX));

    
    x = vectorGetAt(newVector, 0);
    z = vectorGetAt(newVector, 2);
    vectorSetAt(newVector, 2, z * cosf(aboutY) - x * sinf(aboutY));
    vectorSetAt(newVector, 0, z * sinf(aboutY) + x * cosf(aboutY));


    x = vectorGetAt(newVector, 0);
    y = vectorGetAt(newVector, 1);
    vectorSetAt(newVector, 0, x * cosf(aboutZ) - y * sinf(aboutZ));
    vectorSetAt(newVector, 1, x * sinf(aboutZ) + y * cosf(aboutZ));


    return newVector;

}

GLfloat dotProduct(VECTOR_FLOAT* vector1, VECTOR_FLOAT*  vector2) {
    
    GLfloat result = 0;
    

    for (int i = 0; i < vector1->numberOfCoordinates; i++) {
        result += vectorGetAt(vector1, i) * vectorGetAt(vector2, i);
    }


    return result;

}

GLfloat mixedProduct(GLint dimension, ...) {
    
    //  Set up each row of the determinant
    //  to vector from arguments list
    

    va_list args;
    va_start(args, dimension);


    MATRIX_FLOAT* res = createMatrix(dimension, dimension, GL_TRUE, 0, 0);
    for (int i = 0; i < dimension; i++) {
        setRow(res, i, va_arg(args, VECTOR_FLOAT*));
    }


    GLfloat result = determinant(res);


    free(res);


    return result;

}

GLfloat length(VECTOR_FLOAT* vector) {
    
    //  Calculate the length of given vector
    

    GLfloat len = 0.0f;
    for (int i = 0; i < vector->numberOfCoordinates; i++) {
        len += pow(vectorGetAt(vector, i), 2.0f);
    }


    return len;

}

GLfloat determinant(MATRIX_FLOAT* matrix) {
    
    //  Calculate determinant:
    //  if matrix size is 2 just return determinant value,
    //  otherwise we run recursive determinant calculation

    
    if (matrix->rows == 2) {
        return matrixGetAt(matrix, 0, 0) * matrixGetAt(matrix, 1, 1) - matrixGetAt(matrix, 0, 1) * matrixGetAt(matrix, 1, 0);
    }


    GLfloat result = 0;


    //  Construct determinant
    GLfloat cofactor = 1.0f;
    MATRIX_FLOAT* minor = createMatrix(matrix->rows - 1, matrix->rows - 1, GL_TRUE, 0, 0);
    for (int i = 0; i < matrix->rows; i++) {
        for (int j = 1; j < matrix->rows; j++) {
            for (int k = 0; k < i; k++) {
                matrixSetAt(minor, j - 1, k, matrixGetAt(matrix, j, k));
            }
            for (int k = i + 1; k < matrix->rows; k++) {
                matrixSetAt(minor, j - 1, k - 1, matrixGetAt(matrix, j, k));
            }
        }
        result += cofactor * matrixGetAt(matrix, 0, i) * determinant(minor);
        cofactor = -cofactor;
    }
    free(minor);


    return result;

}

GLfloat* getMatrixPointer(MATRIX_FLOAT* matrix) {
    
    return matrix->elements;

}

GLfloat* getVectorPointer(VECTOR_FLOAT* vector) {
    
    return vector->coordinates;

}
